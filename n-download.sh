#!/bin/sh

root=$(pwd)

wget -q -N https://gitlab.com/Kwoth/nadeko-bash-installer/-/raw/v5/detectOS.sh
bash detectOS.sh > /dev/null || exit $?


# remove old backup but store the database
if [ -d nadekobot_old/output/data/ ]; then
    if [ -f nadekobot_old/output/data/NadekoBot.db ]; then
        if [ ! -d nadekobot_db_backups/ ]; then
            mkdir nadekobot_db_backups
        fi
        date_now=$(date +%s)
        cp nadekobot_old/output/data/NadekoBot.db nadekobot_db_backups/NadekoBot-"$date_now".db

        if [ -f nadekobot_old/output/creds.yml ]; then
            cp nadekobot_old/output/creds.yml nadekobot_db_backups/creds-"$date_now".yml
        fi
    fi
fi
rm -rf nadekobot_old 1>/dev/null 2>&1

# make a new backup
mv -fT nadekobot nadekobot_old 1>/dev/null 2>&1

# clone new version
git clone -b v5 --recursive --depth 1 https://gitlab.com/kwoth/nadekobot

wget -q -N https://gitlab.com/kwoth/nadeko-bash-installer/-/raw/v5/rebuild.sh
bash rebuild.sh

cd "$root"
rm "$root/n-download.sh"
exit 0
